-- Common Table Expression (CTE) to calculate sales for each subcategory and year
WITH SubcategorySales AS (
    SELECT
        prod_subcategory,
        year,
        SUM(sales) AS total_sales,
        LAG(SUM(sales)) OVER (PARTITION BY prod_subcategory ORDER BY year) AS prev_year_sales
    FROM
        your_sales_table
    WHERE
        year BETWEEN 1998 AND 2001
    GROUP BY
        prod_subcategory, year
)

-- Selecting subcategories with consistently higher sales compared to the previous year
SELECT DISTINCT
    prod_subcategory
FROM
    SubcategorySales
WHERE
    total_sales > COALESCE(prev_year_sales, 0);