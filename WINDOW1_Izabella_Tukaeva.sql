-- Common Table Expression (CTE) to rank customers based on total sales
WITH RankedCustomers AS (
    SELECT
        customer_id,
        sales_channel,
        total_sales,
        RANK() OVER (PARTITION BY sales_channel ORDER BY total_sales DESC) AS sales_rank
    FROM
        your_sales_table
    WHERE
        -- Add conditions for the specific years
        year IN (1998, 1999, 2001)
)

-- Selecting top 300 customers for each sales channel
SELECT
    customer_id,
    sales_channel,
    total_sales
FROM
    RankedCustomers
WHERE
    sales_rank <= 300;